package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.Command;

public class CreatePersonCommand implements Command {

  private String firstName;
  private String lastName;

  public CreatePersonCommand(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
}
