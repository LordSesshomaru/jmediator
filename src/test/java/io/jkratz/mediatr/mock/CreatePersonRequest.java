package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.Request;

public class CreatePersonRequest implements Request<Integer> {

  private String firstName;
  private String lastName;

  public CreatePersonRequest(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
}
