package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.EventHandler;

public class PersonCreatedEventHandler implements EventHandler<PersonCreatedEvent> {

  @Override
  public void handle(PersonCreatedEvent event) {
    System.out.println(String.format("Person created with ID %s", event.getId()));
  }
}
