package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.Event;

import java.util.UUID;

public class PersonCreatedEvent implements Event {

  private UUID id;

  public PersonCreatedEvent(UUID id) {
    this.id = id;
  }

  public UUID getId() {
    return id;
  }
}
