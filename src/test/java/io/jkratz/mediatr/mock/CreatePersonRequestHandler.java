package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.RequestHandler;

public class CreatePersonRequestHandler implements RequestHandler<CreatePersonRequest, Integer> {

  @Override
  public Integer handle(CreatePersonRequest request) {
    return 42;
  }
}
