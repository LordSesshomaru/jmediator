package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.EventHandler;

public class AlternativePersonCreatedEventHandler implements EventHandler<PersonCreatedEvent> {

  @Override
  public void handle(PersonCreatedEvent event) {
    System.out.println("I do nothing and just waste CPU cycles");
  }
}
