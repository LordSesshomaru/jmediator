package io.jkratz.mediatr.mock;

import io.jkratz.mediatr.api.CommandHandler;

public class CreatePersonCommandHandler implements CommandHandler<CreatePersonCommand> {

  @Override
  public void handle(CreatePersonCommand command) {
    System.out.println(String.format("Command to create person with firstname %s and lastname %s", command.getFirstName(), command.getLastName()));
  }
}
