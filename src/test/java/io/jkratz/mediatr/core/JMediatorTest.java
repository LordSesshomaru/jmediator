package io.jkratz.mediatr.core;

import io.jkratz.mediatr.api.Command;
import io.jkratz.mediatr.api.CommandHandler;
import io.jkratz.mediatr.api.Event;
import io.jkratz.mediatr.api.EventHandler;
import io.jkratz.mediatr.api.Request;
import io.jkratz.mediatr.api.RequestHandler;
import io.jkratz.mediatr.mock.AlternativePersonCreatedEventHandler;
import io.jkratz.mediatr.mock.CreatePersonCommand;
import io.jkratz.mediatr.mock.CreatePersonCommandHandler;
import io.jkratz.mediatr.mock.CreatePersonRequest;
import io.jkratz.mediatr.mock.CreatePersonRequestHandler;
import io.jkratz.mediatr.mock.PersonCreatedEvent;
import io.jkratz.mediatr.mock.PersonCreatedEventHandler;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class JMediatorTest {

  private JMediator mediator;
  private CreatePersonRequestHandler createPersonRequestHandler;
  private CreatePersonCommandHandler createPersonCommandHandler;
  private PersonCreatedEventHandler personCreatedEventHandler;
  private AlternativePersonCreatedEventHandler alternativePersonCreatedEventHandler;

  @Before
  public void beforeTest() {
    this.mediator = new JMediator();
    this.alternativePersonCreatedEventHandler = spy(AlternativePersonCreatedEventHandler.class);
    this.personCreatedEventHandler = spy(PersonCreatedEventHandler.class);
    this.createPersonCommandHandler = spy(CreatePersonCommandHandler.class);
    this.createPersonRequestHandler = spy(CreatePersonRequestHandler.class);
  }

  @Test
  public void testConstructor_withExecutor() {
    Executor executor = Executors.newFixedThreadPool(8);
    JMediator mediator = new JMediator(executor);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_withNullExecutor() {
    new JMediator(null);
  }

  @Test
  public void testRegisterCommandHandler() {
    CommandHandler<CreatePersonCommand> handler = new CreatePersonCommandHandler();
    this.mediator.registerCommandHandler(CreatePersonCommand.class, handler);
    Map<Class<? extends Command>, CommandHandler> handlers = mediator.getCommandHandlers();
    CommandHandler foundHandler = handlers.get(CreatePersonCommand.class);
    assertNotNull(foundHandler);
    assertSame(handler, foundHandler);
    assertEquals(1, handlers.size());
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterCommandHandler_nullType() {
    this.mediator.registerCommandHandler(null, new CreatePersonCommandHandler());
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterCommandHandler_nullHandler() {
    this.mediator.registerCommandHandler(CreatePersonCommand.class, null);
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterCommandHandler_duplicateHandlers() {
    CommandHandler<CreatePersonCommand> handler = command -> { };
    this.initializeMediator();
    this.mediator.registerCommandHandler(CreatePersonCommand.class, handler);
  }

  @Test
  public void testRegisterRequestHandler() {
    RequestHandler<CreatePersonRequest, Integer> handler = new CreatePersonRequestHandler();
    this.mediator.registerRequestHandler(CreatePersonRequest.class, handler);
    Map<Class<? extends Request>, RequestHandler> handlers = this.mediator.getRequestHandlers();
    RequestHandler foundHandler = handlers.get(CreatePersonRequest.class);
    assertNotNull(foundHandler);
    assertSame(handler, foundHandler);
    assertEquals(1, handlers.size());
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterRequestHandler_nullType() {
    this.mediator.registerRequestHandler(null, new CreatePersonRequestHandler());
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterRequestHandler_nullHandler() {
    this.mediator.registerRequestHandler(CreatePersonRequest.class, null);
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterRequestHandler_duplicateHandlers() {
    RequestHandler<CreatePersonRequest, Integer> handler = request -> 42;
    this.initializeMediator();
    this.mediator.registerRequestHandler(CreatePersonRequest.class, handler);
  }

  @Test
  public void testRegisterEventHandler() {
    EventHandler<PersonCreatedEvent> handler1 = new PersonCreatedEventHandler();
    EventHandler<PersonCreatedEvent> handler2 = new AlternativePersonCreatedEventHandler();
    this.mediator.registerEventHandler(PersonCreatedEvent.class, handler1);
    this.mediator.registerEventHandler(PersonCreatedEvent.class, handler2);
    Map<Class<? extends Event>, Set<EventHandler>> handlers = this.mediator.getEventHandlers();
    Set<EventHandler> foundHandlers = handlers.get(PersonCreatedEvent.class);
    assertNotNull(foundHandlers);
    assertEquals(2, foundHandlers.size());
    assertEquals(1, handlers.size());
    assertTrue(foundHandlers.contains(handler1));
    assertTrue(foundHandlers.contains(handler2));
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterEventHandler_nullType() {
    EventHandler<PersonCreatedEvent> handler = new PersonCreatedEventHandler();
    this.mediator.registerEventHandler(null, handler);
  }

  @Test(expected = HandlerRegistrationException.class)
  public void testRegisterEventHandler_nullHandler() {
    this.mediator.registerEventHandler(PersonCreatedEvent.class, null);
  }

  @Test
  public void unregisterCommandHandler() {
    this.initializeMediator();
    this.mediator.unregisterCommandHandler(null);
    this.mediator.unregisterCommandHandler(CreatePersonCommand.class);
    assertEquals(0, this.mediator.getCommandHandlers().size());
  }

  @Test
  public void unregisterRequestHandler() {
    this.initializeMediator();
    this.mediator.unregisterRequestHandler(null);
    this.mediator.unregisterRequestHandler(CreatePersonRequest.class);
    assertEquals(0, this.mediator.getRequestHandlers().size());
  }

  @Test
  public void testUnregisterEventHandler_byType() {
    this.initializeMediator();
    this.mediator.unregisterEventHandler(null);
    this.mediator.unregisterEventHandler(PersonCreatedEvent.class);
    assertEquals(0, this.mediator.getEventHandlers().size());
  }

  @Test
  public void testUnregisterEventHandler_byTypeAndHandler() {
    this.initializeMediator();
    mediator.unregisterEventHandler(PersonCreatedEvent.class, PersonCreatedEventHandler.class);
  }

  @Test
  public void testDispatchCommand() {
    this.initializeMediator();
    CreatePersonCommand command = new CreatePersonCommand("John", "Doe");
    this.mediator.dispatch(command);
    verify(this.createPersonCommandHandler, times(1)).handle(command);
  }

  @Test(expected = MediatorException.class)
  public void testDispatchCommand_nullCommand() {
    this.initializeMediator();
    Command command = null;
    this.mediator.dispatch(command);
  }

  @Test(expected = NoHandlerException.class)
  public void testDispatchCommand_noHandlerAvailable() {
    CreatePersonCommand command = new CreatePersonCommand("John", "Doe");
    this.mediator.dispatch(command);
  }

  @Test
  public void testDispatchCommandAsync() {
    this.initializeMediator();
    CreatePersonCommand command = new CreatePersonCommand("John", "Doe");
    this.mediator.dispatchAsync(command)
        .join();
    verify(this.createPersonCommandHandler, times(1)).handle(command);
  }

  @Test
  public void testDispatchRequest() {
    this.initializeMediator();
    CreatePersonRequest request = new CreatePersonRequest("John", "Doe");
    int result = this.mediator.dispatch(request);
    assertEquals(42, result);
    verify(this.createPersonRequestHandler, times(1)).handle(request);
  }

  @Test(expected = MediatorException.class)
  public void testDispatchRequest_nullRequest() {
    this.initializeMediator();
    Request<Integer> request = null;
    this.mediator.dispatch(request);
  }

  @Test(expected = NoHandlerException.class)
  public void testDispatchRequest_noHandler() {
    CreatePersonRequest request = new CreatePersonRequest("John", "Doe");
    this.mediator.dispatch(request);
  }

  @Test
  public void testDispatchRequestAsync() {
    this.initializeMediator();
    CreatePersonRequest request = new CreatePersonRequest("John", "Doe");
    int result = this.mediator.dispatchAsync(request)
        .join();
    assertEquals(42, result);
    verify(this.createPersonRequestHandler, times(1)).handle(request);
  }

  @Test
  public void testDispatchEvent() {
    this.initializeMediator();
    PersonCreatedEvent event = new PersonCreatedEvent(UUID.randomUUID());
    this.mediator.dispatch(event);
    verify(this.personCreatedEventHandler, times(1)).handle(event);
    verify(this.alternativePersonCreatedEventHandler, times(1)).handle(event);
  }

  @Test(expected = MediatorException.class)
  public void testDispatchEvent_nullEvent() {
    this.initializeMediator();
    Event event = null;
    this.mediator.dispatch(event);
  }

  @Test(expected = NoHandlerException.class)
  public void testDispatchEvent_noHandlers() {
    PersonCreatedEvent event = new PersonCreatedEvent(UUID.randomUUID());
    this.mediator.dispatch(event);
  }

  @Test
  public void testDispatchEventAsync() {
    this.initializeMediator();
    PersonCreatedEvent event = new PersonCreatedEvent(UUID.randomUUID());
    this.mediator.dispatchAsync(event).join();
    verify(this.personCreatedEventHandler, times(1)).handle(event);
    verify(this.alternativePersonCreatedEventHandler, times(1)).handle(event);
  }

  @Test(expected = MediatorException.class)
  public void testDispatchEventAsync_nullEvent() {
    this.initializeMediator();
    Event event = null;
    this.mediator.dispatchAsync(event).join();
  }

  @Test(expected = NoHandlerException.class)
  public void testDispatchEventAsync_noHanlders() {
    PersonCreatedEvent event = new PersonCreatedEvent(UUID.randomUUID());
    this.mediator.dispatchAsync(event).join();
  }

  private void initializeMediator() {
    mediator.registerCommandHandler(CreatePersonCommand.class, this.createPersonCommandHandler);
    mediator.registerRequestHandler(CreatePersonRequest.class, this.createPersonRequestHandler);
    mediator.registerEventHandler(PersonCreatedEvent.class, this.personCreatedEventHandler);
    mediator.registerEventHandler(PersonCreatedEvent.class, this.alternativePersonCreatedEventHandler);
  }
}
