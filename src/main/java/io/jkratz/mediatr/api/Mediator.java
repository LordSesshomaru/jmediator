package io.jkratz.mediatr.api;

import io.jkratz.mediatr.core.NoHandlerException;

import java.util.concurrent.CompletableFuture;

/**
 * A mediator mediates communication between different components within an application.
 * Messages in the form of {@link Command}, {@link Request}, and {@link Event} are dispatched
 * to their associated handler objects for execution. This allows for components and code to
 * remain decoupled.
 *
 * @author Joseph Kratz
 */
public interface Mediator {

    /**
     * Dispatches a {@link Command} to be executed
     *
     * @param command the command to be dispatched and executed
     * @param <T> the type of the command
     */
    <T extends Command> void dispatch(T command);

    /**
     * Dispatches a {@link Command} to be executed asynchronously
     *
     * @param command the command to be dispatched and executed
     * @param <T> the type of the command
     * @return a {@link CompletableFuture} to be fulfilled
     */
    <T extends Command> CompletableFuture<Void> dispatchAsync(T command);

    /**
     * Dispatches a {@link Request} to be executed
     *
     * @param request the request to be dispatched and executed
     * @param <T> the type of the command
     * @param <R> the type of the return value
     * @return the result of the execution of the request
     */
    <T extends Request<R>, R> R dispatch(T request);

    /**
     * Dispatches a {@link Request} to be executed asynchronously
     *
     * @param request the request to be dispatched and executed
     * @param <T> the type of the command
     * @param <R> the type of the return value
     * @return a {@link CompletableFuture} to be fulfilled
     */
    <T extends Request<R>, R> CompletableFuture<R> dispatchAsync(T request);

    /**
     * Dispatches a {@link Event} to be handled/executed
     *
     * @param event the event to be emitted
     * @param <T> the type of the event
     */
    <T extends Event> void dispatch(T event);

    /**
     * Dispatches a {@link Event} to be handled/executed asynchronously
     *
     * @param event the event to be emitted
     * @param <T> the type of the event
     * @return a promise to be fulfilled in the form of a {@link CompletableFuture}
     */
    <T extends Event> CompletableFuture<Void> dispatchAsync(T event);
}
