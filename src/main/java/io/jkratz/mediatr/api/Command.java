package io.jkratz.mediatr.api;

/**
 * Command is a marker interface.
 *
 * @author Joseph Kratz
 */
public interface Command {
}
