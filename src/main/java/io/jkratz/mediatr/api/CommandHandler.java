package io.jkratz.mediatr.api;

/**
 * CommandHandler is a handler that processes a command.
 *
 * @param <T> The type of command
 */
public interface CommandHandler<T extends Command> {

    /**
     * Handles and processes a command
     *
     * @param command the command
     */
    void handle(T command);
}
