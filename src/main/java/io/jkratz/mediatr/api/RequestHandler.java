package io.jkratz.mediatr.api;

/**
 *
 * @param <T>
 * @param <R>
 */
public interface RequestHandler<T extends Request<R>, R> {

    R handle(T request);
}
