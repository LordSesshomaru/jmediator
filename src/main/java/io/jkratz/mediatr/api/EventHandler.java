package io.jkratz.mediatr.api;

/**
 *
 *
 * @param <T>
 */
public interface EventHandler<T extends Event> {

    void handle(T event);
}
