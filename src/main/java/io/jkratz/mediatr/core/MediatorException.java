package io.jkratz.mediatr.core;

/**
 *
 */
public class MediatorException extends RuntimeException {

    public MediatorException() {
        super();
    }

    public MediatorException(String message) {
        super(message);
    }

    public MediatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public MediatorException(Throwable cause) {
        super(cause);
    }

    protected MediatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
