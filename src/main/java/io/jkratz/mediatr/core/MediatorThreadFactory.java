package io.jkratz.mediatr.core;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class MediatorThreadFactory implements ThreadFactory {

    private final AtomicInteger threadNumber = new AtomicInteger(0);

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setName(String.format("jmediator-%d", threadNumber.getAndIncrement()));
        thread.setDaemon(true);
        return thread;
    }
}
