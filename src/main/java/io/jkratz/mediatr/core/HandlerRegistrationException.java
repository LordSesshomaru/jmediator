package io.jkratz.mediatr.core;

/**
 *
 */
public class HandlerRegistrationException extends MediatorException {

    public HandlerRegistrationException() {
        super();
    }

    public HandlerRegistrationException(String message) {
        super(message);
    }

    public HandlerRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandlerRegistrationException(Throwable cause) {
        super(cause);
    }

    protected HandlerRegistrationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
