package io.jkratz.mediatr.core;

import io.jkratz.mediatr.api.Command;
import io.jkratz.mediatr.api.CommandHandler;
import io.jkratz.mediatr.api.Event;
import io.jkratz.mediatr.api.EventHandler;
import io.jkratz.mediatr.api.Mediator;
import io.jkratz.mediatr.api.Request;
import io.jkratz.mediatr.api.RequestHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * JMediator is an implementation of the Mediator interface. JMediator allows registering
 * classes/types to associated handler/s.
 *
 * <p>{@link Request} may be registered with a single {@link RequestHandler}</p>
 * <p>{@link Command} may be registered with a single {@link CommandHandler}</p>
 * <p>{@link Event} may be registered with one or many {@link EventHandler}</p>
 *
 * Mediator is thread safe and can be safely used in concurrent environments.
 *
 * @author Joseph Kratz
 * @see Mediator
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class JMediator implements Mediator {

    private static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    private final Map<Class<? extends Request>, RequestHandler> requestHandlerMap = new ConcurrentHashMap<>();
    private final Map<Class<? extends Command>, CommandHandler> commandHandlerMap = new ConcurrentHashMap<>();
    private final Map<Class<? extends Event>, Set<EventHandler>> eventHandlerMap = new ConcurrentHashMap<>();
    private final Executor executor;

    /**
     * Constructs a new JMediator with a default ThreadPoolExecutor for asynchronous
     * operations.
     *
     * The default ThreadPoolExecutor is ideal for IO bound operations. The
     * thread pool size is the amount of available processors multiplied by 10.
     * In the event the ThreadPoolExecutor rejects a task it will be executed on
     * the calling thread. All threads will be pre-started.
     */
    public JMediator() {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                (AVAILABLE_PROCESSORS * 10),
                (AVAILABLE_PROCESSORS * 10),
                0L,
                TimeUnit.MILLISECONDS,
                new SynchronousQueue<>(),
                new MediatorThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
        executor.prestartAllCoreThreads();
        this.executor = executor;
    }

    /**
     * Constructs a new JMediator with a provided Executor. The executor is used
     * for asynchronous operations.
     *
     * @param executor The executor to be used for dispatching/executing asynchronously
     * @throws IllegalArgumentException when the executor is null
     */
    public JMediator(Executor executor) {
        if (executor == null) {
            throw new IllegalArgumentException("executor cannot be null");
        }
        this.executor = executor;
    }

    /**
     * Returns the request handlers registered as a map of Class -> RequestHandler.
     * The map returned is a UnmodifiableMap. Query operations on the returned map
     * "read through" to the specified map, and attempts to modify the returned map,
     * whether direct or via its collection views, result in an UnsupportedOperationException.
     *
     * @return the map of Request to RequestHandler
     */
    public Map<Class<? extends Request>, RequestHandler> getRequestHandlers() {
        return Collections.unmodifiableMap(requestHandlerMap);
    }

    /**
     * Returns the command handlers registered as a map of Class -> CommandHandler.
     * The map returned is a UnmodifiableMap. Query operations on the returned map
     * "read through" to the specified map, and attempts to modify the returned map,
     * whether direct or via its collection views, result in an UnsupportedOperationException.
     *
     * @return the map of Command to CommandHandler
     */
    public Map<Class<? extends Command>, CommandHandler> getCommandHandlers() {
        return Collections.unmodifiableMap( commandHandlerMap);
    }

    /**
     * Returns the command handlers registered as a map of Event -> Set<EventHandler>
     * The map returned is a UnmodifiableMap. Query operations on the returned map
     * "read through" to the specified map, and attempts to modify the returned map,
     * whether direct or via its collection views, result in an UnsupportedOperationException.
     *
     * @return the map of Event to EventHandler
     */
    public Map<Class<? extends Event>, Set<EventHandler>> getEventHandlers() {
        Map<Class<? extends Event>, Set<EventHandler>> tempEventHandlerMap = new HashMap<>();
        eventHandlerMap.forEach((type, eventHandlers) -> tempEventHandlerMap.put(type, Collections.unmodifiableSet(eventHandlerMap.get(type))));
        return Collections.unmodifiableMap(tempEventHandlerMap);
    }

    /**
     * Registers a RequestHandler to handle a Request.
     *
     * @param type the type of request to register
     * @param requestHandler the handler to handle the request
     * @throws HandlerRegistrationException if type or requestHandler are null or if
     *                                      the type already has a registered RequestHandler
     */
    public void registerRequestHandler(Class<? extends Request> type, RequestHandler requestHandler) {
        if (type == null) throw new HandlerRegistrationException("Invalid Type, cannot register NULL Request");
        if (requestHandler == null) throw new HandlerRegistrationException("Invalid RequestHandler, cannot register a NULL RequestHandler");
        if (requestHandlerMap.containsKey(type)) {
            throw new HandlerRegistrationException(String.format("Type %s already has a registered RequestHandler. A Request may only have a single RequestHandler", type.getCanonicalName()));
        }
        requestHandlerMap.put(type, requestHandler);
    }

    /**
     * Registers a CommandHandler to handle a Command.
     *
     * @param type the type of command to register
     * @param commandHandler the handler to handle the command
     * @throws HandlerRegistrationException if type or commandHandler are null or if
     *                                      the type already has a registered CommandHandler
     */
    public void registerCommandHandler(Class<? extends Command> type, CommandHandler commandHandler) {
        if (type == null) throw new HandlerRegistrationException("Invalid Type, cannot register NULL Command");
        if (commandHandler == null) throw new HandlerRegistrationException("Invalid CommandHandler, cannot register a NULL CommandHandler");
        if (commandHandlerMap.containsKey(type)) {
            throw new HandlerRegistrationException(String.format("Type %s already has a registered CommandHandler. A Command may only have a single CommandHandler", type.getCanonicalName()));
        }
        commandHandlerMap.put(type, commandHandler);
    }

    /**
     * Registers a EventHandler to handle an Event.
     *
     * @param type the type of the event to register
     * @param eventHandler the handler to handle the event
     * @throws HandlerRegistrationException if type or eventHandler are null
     */
    public void registerEventHandler(Class<? extends Event> type, EventHandler eventHandler) {
        if (type == null) throw new HandlerRegistrationException("Invalid Type, cannot register NULL Event");
        if (eventHandler == null) throw new HandlerRegistrationException("Invalid EventHandler, cannot register a NULL EventHandler");
        if (!eventHandlerMap.containsKey(type)) {
            eventHandlerMap.put(type, new CopyOnWriteArraySet<>());
        }
        eventHandlerMap.get(type).add(eventHandler);
    }

    /**
     * Removes the handler for the given Request type. If the provided type is null
     * or isn't registered this is no-op.
     *
     * @param type
     */
    public void unregisterRequestHandler(Class<? extends Request> type) {
        if (type == null) {
            return;
        }
        requestHandlerMap.remove(type);
    }

    /**
     * Removes the handler for the given Command type. If the provided type is null
     * or isn't registered this is no-op.
     *
     * @param type
     */
    public void unregisterCommandHandler(Class<? extends Command> type) {
        if (type == null) {
            return;
        }
        commandHandlerMap.remove(type);
    }

    /**
     * Removes all handlers for the given Event type. If the provided type is null
     * or isn't registered this is no-op.
     *
     * @param type
     */
    public void unregisterEventHandler(Class<? extends Event> type) {
        if (type == null) {
            return;
        }
        eventHandlerMap.remove(type);
    }

    /**
     * Removes a specific handler for the given Event type. If the provided type is null
     * or the eventHandler is null, or the event isn't registered, or the eventHandler
     * isn't registered for the given event this is a no-op.
     *
     * @param type
     * @param eventHandlerType
     */
    public void unregisterEventHandler(Class<? extends Event> type, Class<? extends EventHandler> eventHandlerType) {
        if (type == null || eventHandlerType == null) {
            return;
        }
        Optional.ofNullable(eventHandlerMap.get(type))
            .ifPresent(eventHandlers -> eventHandlers.forEach(h -> {
                if (h.getClass().equals(eventHandlerType)) {
                    eventHandlers.remove(h);
                }
            }));
    }

    /**
     *
     * @param command the command to be dispatched and executed
     * @param <T>
     * @throws MediatorException when the command is null
     * @throws NoHandlerException when there are no handlers available to handle the command
     */
    @Override
    public <T extends Command> void dispatch(T command) {
        if (command == null) {
            throw new MediatorException("Cannot dispatch NULL command");
        }
        CommandHandler<T> handler = commandHandlerMap.get(command.getClass());
        if (handler == null) {
            throw new NoHandlerException(String.format("No CommandHandler for %s", command.getClass().getCanonicalName()));
        }
        handler.handle(command);
    }

    @Override
    public <T extends Command> CompletableFuture<Void> dispatchAsync(T command) {
        return CompletableFuture.runAsync(() -> this.dispatch(command), this.executor);
    }

    @Override
    public <T extends Request<R>, R> R dispatch(T request) {
        if (request == null) {
            throw new MediatorException("Cannot dispatch NULL request");
        }
        RequestHandler<T, R> handler = requestHandlerMap.get(request.getClass());
        if (handler == null) {
            throw new NoHandlerException(String.format("No RequestHandler for %s", request.getClass().getCanonicalName()));
        }
        return handler.handle(request);
    }

    @Override
    public <T extends Request<R>, R> CompletableFuture<R> dispatchAsync(T request) {
        return CompletableFuture.supplyAsync(() -> this.dispatch(request), this.executor);
    }

    @Override
    public <T extends Event> void dispatch(T event) {
        if (event == null) {
            throw new MediatorException("Cannot dispatch NULL event");
        }
        Set<EventHandler> handlers = eventHandlerMap.get(event.getClass());
        if (handlers == null || handlers.size() == 0) {
            throw new NoHandlerException(String.format("No EventHandlers for %s", event.getClass().getCanonicalName()));
        }
        handlers.forEach(h -> h.handle(event));
    }

    @Override
    public <T extends Event> CompletableFuture<Void> dispatchAsync(T event) {
        if (event == null) {
            throw new MediatorException("Cannot dispatch NULL event");
        }
        Set<EventHandler> handlers = eventHandlerMap.get(event.getClass());
        if (handlers == null || handlers.size() == 0) {
            throw new NoHandlerException(String.format("No EventHandlers for %s", event.getClass().getCanonicalName()));
        }
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        handlers.forEach(h -> futures.add(CompletableFuture.runAsync(() -> h.handle(event), this.executor)));
        CompletableFuture<Void>[] completable = futures.toArray(new CompletableFuture[handlers.size()]);
        return CompletableFuture.allOf(completable);
    }
}
